\chapter{Premières notions des groupes} \label{chap:groupe}

  \begin{apropos}{Les groupes sont une notion moderne}
    Bien que \citeauthor{cau82} n'ait pas conscience des notions de groupes telles que nous les connaissons aujourd'hui, il me semble tout à fait à propos d'éclairer son propos par ces définitions actuelles. Ce sont seulement \citeauthor{bet52} et \citeauthor{cay54} qui, selon \cite{ehr13}, étendent dans \citetitle{bet52} et \citetitle{cay54} respectivement les travaux de \citeauthor{gal46a} à une formalisation complète de la théorie des groupes\footnote{\textsc{Caley} fait d'ailleurs une
    référence très concrète à Galois (cf. \cite[p.~41]{cay54}) dans son article, en considérant déjà cette idée comme révolutionnaire.}.
  \end{apropos}

\section{Définitions générales}

  \begin{definition}[Groupe] \label{def:groupe}
    Soit $G$ un ensemble et $\otimes$ une loi de composition. Si $\otimes$ vérifie les conditions suivantes:
    \begin{itemize}
      \item $\otimes$ est associative,
      \item il existe un élément neutre dans $G$ pour $\otimes$,
      \item pour tout $x$ dans $G$, il existe un inverse pour la loi $\otimes$ dans $G$,
    \end{itemize}
    alors $G, \otimes$ est un \emph{groupe}, que l'on note $(G, \otimes)$.
  \end{definition}

  \begin{example}[Exemple de groupe\cf{2}{127}{gre80}] \label{ex:groupe}
    Étudions les symétries et les rotations d'un triangle équilatéral.
    \begin{center}
      \includegraphics[width=.3\textwidth]{triangle}
    \end{center}

    On remarque les axes de symétrie $a,b$ et $c$, que l'on notera $S_a, S_b, S_c$, et il y a deux rotations possibles, vers la droite, et vers la gauche, $R_1, R_2$, ainsi qu'aucun mouvement, $R_0$. On les composera avec $\otimes$, où la notation $a \otimes b$ applique $b$, puis $a$.

    Vérifions les axiomes:
    \paragraph{Associativité} Il faut vérifier trois-à-trois si les combinaisons sont associatives, cela est en effet vérifié.
    \paragraph{Élément neutre} Il s'agit de $R_0$.
    \paragraph{Chaque élément a un inverse selon $R_0$} Pour les symétries, il s'agit de elles-mêmes, pour $R_1$, il s'agit de $R_2$ et inversement.

    On a donc défini le groupe de rotations et de symétries du triangle.
  \end{example}

  \begin{center}
    \includegraphics[width=.4\textwidth]{group-work}
    \\
    \vspace{.5cm}
    \emph{Brown sharpie, Group work\footnote{Extrait de \url{http://brownsharpie.courtneygibbons.org/?p=371}, original par Courtney Gibbons --- Traduction: Travail de groupe; \og{}Alors les élèves, il est temps de travailler en groupe\fg{}; \og{}Mais professeur, je ne m'associe pas à eux!\fg{}. L'auteure joue sur le sens de \emph{associate}, qui, en anglais, s'applique également aux opérations: si l'élève ne peut pas s'associer (=être associatif) avec les autres, alors il ne peut pas être (dans) un groupe.}}
  \end{center}
  
  \subsection{Rapports aux permutations}
  \begin{theorem}[Groupe de permutation\footnote{Le premier auteur à vraiment considérer cette définition est, de fait, Jordan, dès 1870.}] \label{def:groupe_de_permutation}
    Pour tout ensemble $E$, l'ensemble $\mathscr{S}_E$ et le produit de permutation forment un groupe.
    \begin{proof}Vérifions que chacun des critères est satisfait: 
      \begin{itemize}
        \item associativité: nous avons vérifié cela dans le lemme \ref{lem:assoc_produit_permutation};
        \item élément neutre: il s'agit de $\unit_{\mathscr{S}_E}$;
        \item inversibilité de chaque élément: les fonctions associées de chaque permutation sont bijectives, et il existe donc une fonction réciproque.
      \end{itemize}
    \end{proof}
  \end{theorem}




  \begin{remark}
  Dans la suite de ce document, nous utiliserons au maximum la notion de groupe afin de généraliser les énoncés. 
  \end{remark}

  \section{Quelques résultats utiles sur les groupes}
    Il faut noter que tous les résultats que nous avons montrés plus tôt sur les permutations sont également valables de façon généralisée sur les groupes.

    \subsection{Simplifications}
    \begin{lemma}[Simplification à droite] \label{lem:simpl_droite}
      Soient $x, y, u$ trois éléments de $G$. Alors $x \otimes u = y \otimes u \Rightarrow x = y$.
      \begin{proof}
        Par définition, $x = y$ est équivalent à $x \otimes \unit = y \otimes \unit$, et donc $x \otimes \left(u \otimes u^{-1}\right) = y \otimes \left(u \otimes u^{-1}\right)$. En réécrivant selon l'hypothèse, on a $y \otimes u \otimes u^{-1} = y \otimes u \otimes u^{-1}$, ce qui est en effet vérifié.
      \end{proof}
    \end{lemma}

    \begin{lemma}[Simplification à gauche] \label{lem:simpl_gauche}
      Soient $x, y, u$ trois éléments de $G$. Alors $u \otimes x = u \otimes y \Rightarrow x = y$.
      \begin{proof}
        Voir \ref{lem:simpl_droite}.
      \end{proof}
    \end{lemma}
      
    \begin{apropos}{Caley définit implicitement les simplifications}
      \textsc{Caley}\cf{2}{41}{cay54} définit à la fois les notions d'inverses et de simplifications, à gauche et à droite, implicitement dans son article, en remarquant que pour tout $\theta, \phi, \alpha, \beta \in \mathscr{S}_E$, si $\theta = \phi$, alors $\alpha\theta\beta = \alpha\phi\beta$.
    \end{apropos}
    
  \subsection{Puissances, inverses}
    \begin{lemma}[Inverse de compositions] \label{lem:simpl_inv}
      L'inverse de la composition $x \otimes y$ est la composition de $y$ et $x$, c'est-à-dire,
      \begin{displaymath}
        \left(x \otimes y\right)^{-1} = y^{-1} \otimes x^{-1}
      \end{displaymath}
      \begin{proof}
        Utilisions le lemme \ref{lem:simpl_droite}: $\left(x \otimes y\right)^{-1} \otimes \left(x \otimes y\right) = y^{-1} \otimes x^{-1} \otimes \left(x \otimes y\right)$, que l'on peut réécrire sous la forme $\unit = y^{-1} \otimes x^{-1} \otimes x \otimes y$, et donc $\unit = y^{-1} \otimes \unit \otimes y$, $\unit = y^{-1} \otimes y$ et finalement $\unit = \unit$. 
      \end{proof}
    \end{lemma}

    \begin{definition}[Puissance] \label{def:puissance}
      Dans les groupes à notation multiplicative, on appelle puissance l'opération telle que, pour un élément $x$ et $n$ un entier relatif, $$x^n = \underbrace{x \otimes x \otimes \dotsc \otimes x}_{n \text{ fois}}$$
      
      On définira de façon analogue les puissances négatives, c'est-à-dire $$x^{-n} = \underbrace{x^{-1} \otimes x^{-1} \otimes \dotsc \otimes x^{-1}}_{n \text{ fois}}$$
    \end{definition}


    \section{Tables de Cayley}
    On connaît tous les tables de multiplications et d'addition: ce sont en fait des \emph{tables de Cayley}\cf{2}{41}{cay54}. Définies dans \citetitle{cay54}, elles permettent de visualiser rapidement les compositions sur un ensemble fini, en montrant notamment les propriétés commutatives. 

    Elles se présentent sous forme d'un tableau $(\#E)$\texttt{x}$(\#E)$, et montrent le résultat de l'opération sous forme graphique. 
    
    \begin{example}[Table de Cayley du groupe du triangle] \label{ex:groupe_triangle_cayley}
      On a représenté les valeurs de l'exemple \ref{ex:groupe} sous forme d'une table de Cayley. On voit, comme cette table symétrique selon sa diagonale, que la loi $\otimes$ est commutative, c'est-à-dire que l'expression $a \otimes b$ est équivalent à $b \otimes a$.

    \begin{center}
      \vspace{1cm}
      \begin{tabular}{c||c|c|c|c|c|c|}
        $\otimes$ & $R_0$ & $R_1$ & $R_2$ & $S_a$ & $S_b$ & $S_c$ \\
        \hline
        \hline
        $R_0$     & $R_0$ & $R_1$ & $R_2$ & $S_a$ & $S_b$ & $S_c$ \\
        \hline
        $R_1$     & $R_1$ & $R_2$ & $R_0$ & $S_b$ & $S_c$ & $S_a$ \\
        \hline
        $R_2$     & $R_2$ & $R_0$ & $R_1$ & $S_c$ & $S_a$ & $S_b$ \\
        \hline
        $S_a$     & $S_a$ & $S_b$ & $S_c$ & $R_0$ & $R_2$ & $R_1$ \\
        \hline
        $S_b$     & $S_b$ & $S_c$ & $S_a$ & $R_2$ & $R_0$ & $R_2$ \\
        \hline
        $S_c$     & $S_c$ & $S_a$ & $S_b$ & $R_1$ & $R_2$ & $R_0$ \\
        \hline
      \end{tabular}
      \\
      \vspace{.5cm}
      \emph{Table de Cayley associée aux symétries et aux rotations d'un triangle.}
    \end{center}
    \end{example}

    \begin{apropos}{Cayley utilise les tables dans son article de 1854}
      Cayley est le premier à introduire ces tables dans son article de 1854, afin de montrer à quel point celles-ci sont utiles lorsque l'on cherche à représenter les caractéristiques des opérations.
      \begin{center}
        \includegraphics[width=.8\textwidth]{cay-table}
      \end{center}

      Par exemple, Galois, dans son mémoire, utilise une notation bien moins claire pour représenter les groupes de permutation: 

      \begin{center}
        \includegraphics[width=.8\textwidth]{gal-table}
      \end{center}
      \vspace{.5cm}
    \end{apropos}

  \section{Groupes commutatifs: les groupes abéliens}
    \begin{definition}[Groupe abélien] \label{def:groupe_abelien}
      Soit $(G,\oplus)$ un groupe. Alors $G$ est dit abélien si $\left(\forall (a,b) \in G^2\right)\left(a \oplus b = b \oplus a\right)$. On y préfère une notation additive.
    \end{definition}

    \begin{example}[Groupe abélien] \label{ex:groupe_abelien}
      Le groupe des symétries et des rotations d'un triangle, que l'on a rencontré plus haut, est abélien: en effet, il présente une symétrie diagonale selon sa table de Cayley. Notamment, il est équivalent d'exprimer une symétrie d'axe $a$ plus une rotation vers la gauche qu'une rotation vers la gauche puis une symétrie d'axe $b$. 
    \end{example}

    \begin{example}[Groupe non-abélien] \label{ex:groupe_non_abelien}
      Les groupes de permutation d'ordre $\geq 3$ ne sont pas abéliens, puisqu'ils contiennent tous $S_3$ qui n'est pas abélien. 

      En effet, ses permutations sont les cycles $\unit_3, (1~2), (2~3), (1~3), (1~2~3), (1~3~2)$; construisons la \emph{table de Cayley} associée.

      \begin{center}
      \begin{tabular}{l|l|l|l|l|l|l|}
        $\cdot$   & $\unit_3$ & $(1~2)$   & $(2~3)$   & $(1~3)$   & $(1~2~3)$ & $(1~3~2)$ \\
        \hline
        $\unit_3$ & $\unit_3$ & $(1~2)$   & $(2~3)$   & $(1~3)$   & $(1~2~3)$ & $(1~3~2)$ \\
        \hline
        $(1~2)$   & $(1~2)$   & $\unit_3$ & $(1~2~3)$ & $(1~3~2)$ & $(2~3)$   & $(1~3)$   \\
        \hline
        $(2~3)$   & $(2~3)$   & $(1~3~2)$ & $\unit_3$ & $(1~2~3)$ & $(1~3)$   & $(1~2)$   \\
        \hline
        $(1~3)$   & $(1~3)$   & $(1~2~3)$ & $(1~3~2)$ & $\unit_3$ & $(1~2)$   & $(2~3)$   \\
        \hline
        $(1~2~3)$ & $(1~2~3)$ & $(1~3)$   & $(1~2)$   & $(2~3)$   & $(1~3~2)$ & $\unit_3$ \\
        \hline
        $(1~3~2)$ & $(1~3~2)$ & $(2~3)$   & $(1~3)$   & $(1~2)$   & $\unit_3$ & $(1~2~3)$ \\
        \hline
      \end{tabular}
    \end{center}

    On remarque immédiatement que cette table n'a pas de symétrie et qu'elle n'est donc, par conséquent, pas l'image d'un groupe abélien. On notera au passage que cette remarque conduit, par les remarques de Galois\footnote{cf. \citeauthor{gal46b}, \citetitle{gal46b}}, à ce que les équations du degré cinq ou supérieur ne sont pas solubles par radicaux.
    \end{example}
