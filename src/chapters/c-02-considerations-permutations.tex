\chapter{Espace introductif: les permutations}
\label{chap:cons_perm}
Il peut être un peu curieux de s'intéresser aux permutations dans un travail concernant la notion de groupe; cependant, cela est directement relié à la \emph{théorie des équations}. On ne cherchera pas ici à comprendre quelles sont les applications dans cette théorie, on peut en revanche essayer de comprendre comment les permutations ont pu conduire les pensées des mathématiciens pour arriver à la \emph{théorie des groupes}.

\section{Permutations, produits et éléments neutres} \label{sec:definitions_generales_permutations}
\subsection{Définitions générales}
\begin{definition}[Permutation\cf{2}{66}{cau15a}\cf{1}{172}{cau82}\cf{2}{419}{gal46a}] \label{def:permutation}
    Soit E un ensemble et $\sigma$ une fonction de $E$ vers $E$. Alors $\sigma$ est une permutation de $E$ si $\sigma$ est une bijection de $E$.
    On la note
    \begin{equation*}
      \begin{array}{lllcl}
        \sigma & : & E      & \longrightarrow & E \\
               &   & x_1    & \longmapsto     & p_1 \\
               &   & x_2    & \longmapsto     & p_2 \\
               &   &        & \dots                  & \\
               &   & x_n    & \longmapsto     & p_n \\
      \end{array}
      \text{ ou } 
      \left(
        \begin{array}{cccc}
          x_1 & x_2 & \dots & x_n \\
          p_1 & p_2 & \dots & p_n \\
        \end{array}
      \right) \text{, de façon équivalente.}
    \end{equation*}
  \end{definition} 

  \begin{apropos}{\citeauthor{gal46a} parle de permutations dans son \citetitle{gal46a}}
    Galois ne définit pas la notion de lorsqu'il l'aborde dans son mémoire; cela s'expliquerait, selon \citeauthor{gal11} par le fait que la notion est définie dans \cite{lag70} puis reprise dans \cite{lag08}, qu'il a certainement consulté\cf{3}{1}{gal11}, et dont ses travaux sont souvent cités comme une prolongation directe.
  \end{apropos}

  \begin{example} 
    Par exemple,  
    \begin{displaymath}
      \left(
        \begin{array}{ccccc}
          1 & 2 & 3 & 4 & 5 \\
          3 & 4 & 5 & 1 & 2 \\
        \end{array}
      \right),
      \left(
        \begin{array}{ccccc}
          3 & 1 & 4 & 2 & 5 \\
          5 & 3 & 1 & 4 & 2 \\
        \end{array}
      \right)
    \end{displaymath} sont deux permutations, que l'on remarque strictement équivalentes. 
    En revanche, 
    \begin{displaymath}
      \left(
        \begin{array}{ccccccc}
          1 & 2 & 3 & 4 & 5 & 6 & 7 \\
          2 & 3 & 3 & 5 & 2 & 7 & 6 \\
        \end{array}
      \right)
    \end{displaymath} n'est pas une permutation car la fonction qui y est associée n'est pas bijective. Vérifions-le en étudiant sa fonction associée.
    \begin{equation*}
      \begin{array}{lllcl}
        \sigma & : & 1 & \longmapsto & 2 \\
          & : & 2 & \longmapsto & 3 \\
          & : & 3 & \longmapsto & 3 \\
          & : & 4 & \longmapsto & 5 \\
          & : & 5 & \longmapsto & 2 \\
          & : & 6 & \longmapsto & 7 \\
          & : & 7 & \longmapsto & 6 \\
      \end{array}
    \end{equation*}
    En observant la fonction, on remarque que son image est $\{2,3,5,6,7\}$ alors que son domaine est $I_7 = \{1,2,3,4,5,6,7\}$; par conséquent, elle ne peut pas être bijective et n'est donc, par définition, pas une permutation.
  \end{example}


  \begin{apropos}{Notation numérique des permutations}
    La notation numérique\footnote{J'entends par là des sous-ensembles de $\mathbb{N}$ comme domaines et images des permutations.} a été utilisée déjà dans \cite{cau15a}, mais a été abandonnée dans \cite{cau82}. En effet, \citeauthor{gal46a} utilise une notation alphabétique (puisqu'elles font plus sens dans son usage --- coefficients de fonctions polynomiales); cela montre donc l'influence qu'il a eu sur \citeauthor{cau82}. 
    
    On notera que cette notation a été abandonnée depuis, au profit de la lisibilité qu'offre la notation numérique.
  \end{apropos}



  \begin{definition}[Ensemble des permutations d'un ensemble] \label{def:se}
    Soit $E$ un ensemble. On note $\mathscr{S}_E$ l'ensemble des permutations de $E$. 
    
    Lorsque $E = I_n$, on s'autorise la notation $\mathscr{S}_n$. 
  \end{definition}

  \begin{lemma}[Cardinal de $\mathscr{S}_E$\cf{1}{63}{cau15a}\cf{2}{192}{cau82}] \label{lemma:cardinal_se}
    Le cardinal $\#\mathscr{S}_E$ de $\mathscr{S}_E$ est de $(\#E)!$.

    \begin{proof}
      $\mathscr{S}_E$ est l'ensemble des arrangements que l'on peut obtenir sur les éléments de $E$. Cela revient donc à un problème de combinatoire \footnote{Notons que l'on peut donner une preuve qui n'en fait pas usage.} trivial. Nous cherchons en effet les arrangements sans répétition, où l'on arrange $\#E$ éléments dans $\#E$ cases, c'est-à-dire 
      \begin{displaymath}
        A_{\#E}^{\#E} = \frac{(\#E)!}{(\#E-\#E)!} = \frac{(\#E)!}{(0)!} = (\#E)!
      \end{displaymath}
    \end{proof}
  \end{lemma}

\subsection{Produits de permutations}

  \begin{definition}[Produit de permutations\cf{4}{173}{cau82}] \label{def:produit_permutation}
    Soit $\sigma_1, \sigma_2$ deux permutations. On note $\sigma_1\sigma_2$ ou $\sigma_1 \cdot \sigma_2$ la composée $\sigma_1 \circ \sigma_2$ des fonctions associées et on l'appelle \emph{produit de $\sigma_1$ et $\sigma_2$}.
  \end{definition}


  \begin{remark}
    Afin d'effectuer le produit d'une façon plus visuelle, l'on peut écrire $\sigma_1$ sous $\sigma_2$ en alignant les paramètres, on n'a plus qu'à supprimer la ligne centrale afin d'obtenir le produit.
  \end{remark}

  \begin{example}
    Soient $\sigma_1, \sigma_2$ deux permutations telles que
    \begin{equation*}
      \sigma_1 = \left(\begin{array}{ccc}
              1 & 2 & 3 \\
              3 & 1 & 2 \\
            \end{array}\right), 
      \sigma_2 = \left(\begin{array}{ccccc}
              2 & 3 & 4 & 5 & 6 \\
              4 & 6 & 3 & 2 & 5 \\
            \end{array}\right)
    \end{equation*} 
    En écrivant $\sigma_1$ sous $\sigma_2$, on obtient
    \begin{equation*}
      \left(\begin{array}{cccccc}
        1 & 2 & 3 & 4 & 5 & 6 \\
        1 & 4 & 6 & 3 & 2 & 5 \\
        3 & 4 & 6 & 2 & 1 & 5 \\
      \end{array}\right)
    \end{equation*}
    On a donc

    \begin{equation*}
      \sigma_1\sigma_2 = \left(\begin{array}{cccccc}
                  1 & 2 & 3 & 4 & 5 & 6 \\
                  3 & 4 & 6 & 2 & 1 & 5 \\
                \end{array}\right)
    \end{equation*}
  \end{example}
  
  \begin{definition}[Ordre d'une permutation] \label{def:ordre_permutation}
    Soit $\sigma$ une permutation. On définit \emph{l'ordre} de $\sigma$ la plus petite puissance de $\sigma$ telle que celle-ci est est réduite à l'identité. On le note $\text{ord}(\sigma)$, ou, plus simplement, $r$, lorsque cela n'est pas ambiguë.
  \end{definition}
  
  \begin{lemma}[Produit de permutations] \label{lem:produit_permutation} 
    Soient $\sigma_1, \sigma_2$ deux permutations, alors le produit $\sigma_1\sigma_2$ est une permutation.
    \begin{equation*}
      \left(\forall \left(\sigma_1, \sigma_2\right) \in \mathscr{S}_E^2\right)\left(\sigma_1\sigma_2 \in \mathscr{S}_{E}\right)
    \end{equation*}
    \begin{proof}
      Cela découle de la définition fonctionnelle d'une permutation: comme les fonctions associées sont des bijections, leur composition est aussi une bijection.
    \end{proof}
  \end{lemma}

  \begin{apropos}{Aucun auteur n'évoque le lemme \ref{lem:produit_permutation}}
    Tous les auteurs que j'ai consulté semblent ignorer ce lemme qui, pourtant, n'est évident que pour la loi de composition $\circ$. Cette omission est peut-être due à l'origine de cette loi: on a cherché à décomposer une permutation, et donc, par conséquent, le résultat de la composition devait en être une.
  \end{apropos}

  \begin{theorem}[Associativité du produit de permutations] \label{lem:assoc_produit_permutation}
    Soient $\sigma_1, \sigma_2, \sigma_3$ trois permutations. Alors les relations $\sigma_1 \cdot (\sigma_2 \cdot \sigma_3)$ et $(\sigma_1 \cdot \sigma_2) \cdot \sigma_3$ sont égales. On dit du produit de permutations qu'il est associatif. C'est-à-dire,

    \begin{displaymath}
      \left(\forall\left(\sigma_1,\sigma_2,\sigma_3\right) \in \mathscr{S}_E^3\right)\left(\sigma_1\cdot\left(\sigma_2\cdot\sigma_3\right) = \left(\sigma_1\cdot\sigma_2\right)\cdot\sigma_3\right)
    \end{displaymath}

    \begin{proof}
      Rappelons-nous que le produit de permutation peut être vu comme une composition des fonctions associées, soit donc l'égalité $\sigma_1 \circ (\sigma_2 \circ \sigma_3) = (\sigma_1 \circ \sigma_2) \circ \sigma_3$. Comme la composition de fonctions est associative, la propriété est vérifiée.  
    \end{proof}
  \end{theorem}

  \begin{apropos}{Usage de l'associativité}
    Je n'ai pas relevé de mentions de la propriété d'associativité dans les divers articles consultés\footnote{C'est-à-dire dans les ouvrages de \citeauthor{gal46a}, \citeauthor{cau15a}, \citeauthor{lag70} que l'on a cité.}; cependant, quelques uns de leurs résultats en font un usage direct pour leur preuve, à l'inverse de la commutativité qui fait preuve d'une réelle recherche sur ses propriétés. Cependant, dès \textsc{Caley}, cela est évident:
    \begin{center}
      \includegraphics[width=\textwidth]{cal-comm}
    \end{center}
  \end{apropos}

  \begin{theorem}[Non-commutativité du produit de permutations\cf{3}{174}{cau82}\cf{2}{40}{cay54}]\label{lem:non_comm_produit_permutation}
    Le produit de deux permutations n'est pas commutatif\footnote{\cite{cau82} désigne les permutations où le produit est commutatif comme \emph{permutables}. Nous verrons par la suite dans quel cas elles le sont.}.
    \begin{equation*}
      \left(\exists \left(\sigma_1, \sigma_2\right) \in \mathscr{S}_E^2\right)\left(\sigma_1\sigma_2 \neq \sigma_2\sigma_1\right)
    \end{equation*}
    \begin{proof}
    Pour montrer cela, il suffit de trouver un cas où cette propriété n'est pas satisfaite. On peut prendre par exemple 
    \begin{displaymath}
      \sigma_1 = \left(\begin{array}{ccc} 1 & 2 & 3 \\ 2 & 1 & 3\end{array}\right) \text{ et } \sigma_2 = \left(\begin{array}{ccc} 1 & 2 & 3 \\ 1 & 3 & 2 \end{array}\right)
    \end{displaymath}
    On a
    \begin{displaymath}
      \sigma_1\sigma_2 = \left(\begin{array}{ccc}1 & 2 & 3 \\ 2 & 3 & 1\end{array}\right)
    \end{displaymath}
    mais
    \begin{displaymath}
      \sigma_2\sigma_1 = \left(\begin{array}{ccc}1 & 2 & 3 \\ 3 & 1 & 2\end{array}\right)
    \end{displaymath}
    On a donc vérifié que le produit n'est pas commutatif.
    \end{proof}
  \end{theorem}

\subsection{Éléments neutres}
  \begin{definition}[Élément neutre à gauche\cf{2}{175}{cau82}] \label{def:element_neutre_gauche}
    Soit $\sigma_1$ une permutation de $E$. On l'appelle l'élément neutre à gauche si $\left(\forall \sigma_2 \in \mathscr{S}_E\right)\left(\sigma_1\sigma_2 = \sigma_2\right)$. 
  \end{definition}

  \begin{definition}[Élément neutre à droite\cf{2}{175}{cau82}] \label{def:element_neutre_gauche}
    Soit $\sigma_1$ une permutation. On l'appelle l'élément neutre à droite si $\left(\forall \sigma_2 \in \mathscr{S}_E\right)\left(\sigma_2\sigma_1 = \sigma_2\right)$. 
  \end{definition}

  \begin{theorem}[Commutativité et unicité de l'élément neutre\cf{3}{175}{cau82}] \label{def:unicite_element_neutre}
    L'élément neutre à gauche est unique et est strictement égal à l'élément neutre à droite. On note cet élément $\unit_{\mathscr{S}_E}$.

    \begin{proof}
      Soit $\sigma_1, \sigma_2$ deux élément neutres de $\mathscr{S}_E$, respectivement à gauche et à droite. En réécrivant, on obtient $\sigma_1 = \sigma_1\sigma_2$ puis $\sigma_1\sigma_2 = \sigma_2$, donc $\sigma_1 = \sigma_2 = \unit_{\mathscr{S}_E}$. 
    \end{proof}
  \end{theorem}

\section{Inverses de permutations} \label{sec:inverses_permutations}
\begin{definition}[Inverse à droite d'une permutation\cf{1}{182}{cau82}] \label{def:inverse_permutation}
    On définit l'inverse à droite $\sigma^{-1}$ de $\sigma$ la permutation telle que 
    \begin{equation*}
      \sigma\sigma^{-1} = \unit_{\mathscr{S}_n}
    \end{equation*}
    En notation fonctionnelle, on a 
    \begin{equation*}
      \left(\forall x\right)\left(\left(\sigma \circ \sigma^{-1}\right)\left(x\right) = x\right)
    \end{equation*}
  \end{definition}

  \begin{definition}[Inverse à gauche d'une permutation] \label{def:inverse_permutation}
    On définit l'inverse à gauche $\sigma^{-1}'$ de $\sigma$ la permutation telle que 
    \begin{equation*}
      \sigma^{-1}'\sigma = \unit_{\mathscr{S}_n}
    \end{equation*}
  \end{definition}
  
  \begin{apropos}{La différence n'est pas faite entre inverses à gauche et à droite}
    \citeauthor{cau82} ne fait pas la différence entre les permutations \emph{inverses à gauche} et les permutations \emph{inverses à droite} dans son ouvrage de \citeyear{cau82}. En effet, il considérait le théorème \ref{thm:commutativite_inverse_permutations} comme trivial et acquis. Cela était certainement évident pour lui, du fait qu'il semble davantage manipuler les permutations en tant que fonction plutôt qu'en tant qu'objets \og{}nouveaux\fg{}; il ne se gène donc pas de considérer cela comme une simple notation et d'y appliquer des théorèmes généralisés.
  \end{apropos}

  \begin{example}Prenons par exemple la permutation $\sigma = \left(\begin{array}{ccccc}1 & 2 & 3 & 4 & 5\\ 4 & 2 & 1 & 3 & 5\end{array}\right)$. Si l'on répète la méthode énoncée en \ref{sec:definitions_generales_permutations}, on a 
    \begin{displaymath}
    \sigma\sigma^{-1} = \unit_{\mathscr{S}_n} = \left(\begin{array}{ccccc}\cancel{1} & \cancel{2} & \cancel{3} & \cancel{4} & \cancel{5}\\ 4 & 2 & 1 & 3 & 5\\ 1 & 2 & 3 & 4 & 5\end{array}\right)
    \end{displaymath}
      et donc, par conséquent,
    \begin{displaymath}
      \sigma^{-1} = \left(\begin{array}{ccccc}4 & 2 & 1 & 3 & 5\\ 1 & 2 & 3 & 4 & 5\end{array}\right).
    \end{displaymath}
  \end{example}

  \begin{lemma}[Inverse de l'inverse] \label{thm:inverse_inverse}
    Soit $\sigma$ une permutation et $\sigma^{-1}$ son inverse. Alors l'inverse de $\sigma^{-1}$ est $\sigma$.
    
    \begin{proof}
      Rappelons que la fonction $\sigma$ associée à la permutation est une bijection, et donc, on a la propriété, $\left(\sigma^{-1}\right)^{-1} = \sigma^{(-1)(-1)} = \sigma$.
    \end{proof}
  \end{lemma}

  \begin{theorem}[Commutativité de l'inverse] \label{thm:commutativite_inverse_permutations}
    Soit $\sigma_1$ une permutation. Alors $\sigma_1^{-1}$ est égal à $\sigma_1^{-1}'$. 
    \begin{proof}
      Posons $\sigma_2 = \sigma_1^{-1}$. On a, par définition, on a donc $\sigma_2\sigma_2^{-1} = \unit_{\mathscr{S}_n}$, en réécrivant, on a $\sigma_1^{-1}\left(\sigma_1^{-1}\right)^{-1} = \sigma_1^{-1}\sigma_1 = \unit_{\mathscr{S}_n} = \sigma_1^{-1}'\sigma_1$.
    \end{proof}
  \end{theorem}

  \begin{corollary}[Unicité de l'inverse] \label{lem:unicite_inverse_permutations}
    Soit $\sigma$ une permutation. L'inverse de $\sigma$ est unique.
    \begin{proof}
      Soient $\rho_1, \rho_2$ deux inverses de $\sigma$. On a $\rho_1 = \rho_1\unit_{\mathscr{S}_n} = \rho_1\left(\sigma\rho_2\right) = \left(\rho_1\sigma\right)\rho_2 = \unit_{\mathscr{S}_n}\rho_2 = \rho_2$.
    \end{proof}
  \end{corollary}

  \begin{definition}[Puissances de permutations\cf{4}{175}{cau82}] \label{def:puissance_permutation}
    Soit $\sigma$ une permutation. On définit $\sigma^n$ comme la notation désignant le produit $\sigma^n=\underbrace{\sigma\cdot\sigma\cdot\dots\cdot\sigma}_{n \text{ fois}}$.
  \end{definition}

  \begin{lemma}[Puissances semblables\cf{4}{176}{cau82}] \label{lem:top_puissances}
    Soit $n,m$ deux entiers, $\sigma \in \mathscr{S}_E$. Alors si $m=kr+n, k \in \mathbb{N}$ (avec $r$ l'ordre de la permutation), alors $\sigma^n=\sigma^m$.
    \begin{proof}
      Notons $\sigma^m$ comme $\sigma^{kr+n}=\sigma^{kr}\sigma^n$. Il suffit de montrer que $\sigma^{kr}$ est l'identité, particulièrement $\sigma^r$. Or, par définition, $r$ est le plus petit entier qui réduit $\sigma^n$ à l'identité.
    \end{proof}
  \end{lemma}


\section{Permutations particulières} \label{sec:permuta_partic}
Étudions maintenant quelques permutations particulières, que l'on appelle \emph{cycles} et \emph{transpositions}.
  
  \subsection{Cycles} \label{sub:cycles}
  \begin{definition}[Cycle\cf{1}{178}{cau82}] \label{def:cycle}
    Un cycle est une permutation qui amène chacun de ses éléments sur un seul et un seul autre, c'est-à-dire $$\left(\begin{array}{cccccc}1 & \sigma(1) & \sigma(2) & \dotsc & \sigma(n-1) & \sigma(n)\\\sigma(1) & \sigma(2) & \sigma(3) & \dotsc & \sigma(n-1) & 1\end{array}\right)$$ On simplifie la notation par par $\left(1\ \sigma(1)\ \sigma(2)\ \dotsc\ \sigma(n)\right)$.
    \end{definition}

    \begin{example} \label{ex:cycle}
    Un exemple de cycle est $\eta = \left(\begin{array}{cccc}1 & 2 & 3 & 4\\2 & 3 & 4 & 1\end{array}\right)$, que l'on note également $\left(1\ 2\ 3\ 4\right)$. On peut le voir sous une forme graphique par la figure \ref{fig:cycle}. 
    
    \begin{figure}[h!]
      \centering
      \begin{tikzpicture}[scale=1.5]
        \tikzstyle{num} = [circle,fill=black!10]
        \draw[->] (1,0) arc (0:70:1);
        \draw[->] (0,1) arc (90:160:1);
        \draw[->] (-1,0) arc (180:250:1);
        \draw[->] (0,-1) arc (270:340:1);
        \node[num] at (0, 1) {1};
        \node[num] at (-1,0) {2};
        \node[num] at (0,-1) {3};
        \node[num] at (1,0) {4};
        \node at (0.5, 0.5) {$\eta$};
      \end{tikzpicture}
      \caption{Visualisation graphique du cycle $\eta = \left(1\ 2\ 3\ 4\right)$.} \label{fig:cycle}
    \end{figure}
    \end{example}

    \begin{theorem}[Puissances de cycles\cf{1}{179}{cau82}] \label{thm:puissance_cycle}
      La puissance $n$-ième d'un cycle d'ordre $k$ peut s'exprimer au minimum en $l = \text{pgcd}(n,k)$ cycles.
    \end{theorem}

    \begin{example}[Théroème \ref{thm:puissance_cycle}]
      Considérons le cycle $\eta = \left(1\ 2\ 3\ 4\right)$ de $I_4$, on a donc $k = 4$.
      \begin{itemize}
        \item[$\eta^1$] $ = \left(1\ 2\ 3\ 4\right)$. On a également $\text{pgcd}(1;4) = 1$, on vérifie donc le théorème;
      \item[$\eta^2$] $ = \left(\begin{array}{cccc}1 & 2 & 3 & 4\\2 & 3 & 4 & 1\end{array}\right)\cdot\left(\begin{array}{cccc}1 & 2 & 3 & 4\\2 & 3 & 4 & 1\end{array}\right) = \left(\begin{array}{cccc}1 & 2 & 3 & 4\\3 & 4 & 1 & 2\end{array}\right)$. On a $\text{pgcd}(1;4) = 2$, et l'on peut exprimer $\eta^2$ comme le produit de $\left(1\ 3\right)\left(2\ 4\right)$;
        \item[$\eta^3$] $ = \left(\begin{array}{cccc}1 & 2 & 3 & 4\\4 & 1 & 2 & 3\end{array}\right)$, comme le pgcd vaut $\text{pgcd}(3;4) = 1$, on peut l'exprimer comme un cycle: $\left(1\ 4\ 3\ 2\right)$.
      \item[$\eta^4$] $ = \left(\begin{array}{cccc}1 & 2 & 3 & 4\\1 & 2 & 3 & 4\end{array}\right)$, $\text{pgcd}(4;4) = 4$, $\eta^4 = \left(1\right)\left(2\right)\left(3\right)\left(4\right) = \unit_{\mathscr{S}_4}$, avec $\left(n\right)$ une syntaxe plus agréable pour $\left(n\ n\right)$;
        \item[$\eta^5$] $ = \eta^{k+1} = \eta^1 = \left(1\ 2\ 3\ 4\right)$, avec également $\text{pgcd}(5;4) = 1$.
      \end{itemize}
    \end{example}

    \begin{proof}[Théorème \ref{thm:puissance_cycle}]
      On peut voir un cycle comme un cercle sur lequel on aurait inscrit ses éléments. La puissance d'un cycle serait alors un simple décalage de un élément sur le cercle; par conséquent, il ne sera pas toujours possible, selon la puissance, de la représenter en un chemin unique. Il est évident que le nombre de chemins minimal pour toucher chacun des éléments sera exactement égal au plus grand diviseur commun au nombre d'éléments et à la taille des décalages.
    \end{proof}
    
    \begin{example}[Illustration du théorème \ref{thm:puissance_cycle}]
      Comme on le propose dans la preuve, l'on peut visualiser les puissances d'un cycles telles que\footnote{Illustrations: Wolfram Alpha.}:\\
      \begin{center}
      \begin{tabular}{|c|c|c|c|}
        \hline
        $\eta^1$ & $\eta^2$ & $\eta^3$ & $\eta^4$ \\
        \hline
        \includegraphics[width=.2\textwidth]{1-2-3-4-1} &
        \includegraphics[width=.2\textwidth]{1-2-3-4-2} &
        \includegraphics[width=.2\textwidth]{1-2-3-4-3} &
        \includegraphics[width=.1\textwidth]{1-2-3-4-4} \\
        \hline
      \end{tabular}
    \end{center}
    \end{example}
\subsection{Transpositions} \label{sub:transpositions}

    \begin{definition}[Transposition] \label{def:transposition}
      Une transposition est une permutation laissant tous les $I_n$ invariants, à l'exception de deux d'entre eux. On la note $\left(a\ b\right)$.
    \end{definition}

    \begin{example} \label{ex:transposition}
      Par exemple, 

      \begin{displaymath}
        \left(\begin{array}{cccc} 1 & 2 & 3 & 4 \\ 2 & 1 & 3 & 4\end{array}\right) 
      \end{displaymath}
      
      est une transposition (de $I_4$), notée $\left(1\ 2\right)$, mais 

      \begin{displaymath}
        \left(\begin{array}{cccc} 1 & 2 & 3 & 4 \\ 2 & 3 & 1 & 4\end{array}\right)
      \end{displaymath}

      ne l'est pas.
    \end{example}

    \begin{apropos}{La notion de transposition n'existe pas chez Cauchy}
      Cauchy --- et ses contemporains --- ne considèrent pas particulièrement les transpositions en tant que telles mais s'intéressent d'avantage aux cycles. 

      Il obtient par ailleurs des résultats intéressants\cf{3}{183}{cau82} sur ce qu'il désigne \emph{substituions régulières} --- les permutations qui peuvent se décomposer en tant que cycles de ordre, en opposition avec les \emph{substituions irrégulières}, qui peuvent s'exprimer comme le produit de cycles d'ordre non-constant ---, cependant ces notions sont abandonnée par la suite; ces résultats se rapprochent relativement de ceux que l'on obtient en définissant le signe d'une
      permutation\footnote{Une permutation est dite paire si elle peut se décomposer en un nombre pair de transpositions, impaire sinon.}.
    \end{apropos}
    
    \begin{theorem}[Décomposition en produit de transposition] \label{thm:decomposition_transposition}
      Soit $\sigma$ une permutation de $I_n$. Si $n > 1$, alors $\sigma$ peut être écrit comme le produit de transpositions.
    \end{theorem}

    \begin{example}
      Prenons
      \begin{displaymath}
        \sigma = \left(\begin{array}{ccccc} 1 & 2 & 3 & 4 & 5\\
                  2 & 5 & 4 & 3 & 1\end{array}\right)
      \end{displaymath}
    et cherchons une décomposition de $\sigma$. La première est frappante: $\left(3\ 4\right)$. On remarque également que le produit $\left(1\ 2\right) \cdot \left(2\ 5\right)$ vaut $\left(\begin{array}{ccc}1 & 2 & 5\\2 & 5 & 1\end{array}\right)$, et on a donc comme décomposition $\left(3\ 4\right) \cdot \left(1\ 2\right) \cdot \left(2\ 5\right)$.
    \end{example}

    \begin{proof}[Théorème \ref{thm:decomposition_transposition}]
      La preuve est évidente pour $n$ fini: les permutations de $I_2$ sont exactement des transpositions, on peut former chaque permutations de $I_3$ en combinant deux permutations de $I_2$, puis celles de $I_4$ en combinant une de $I_3$ et une de $I_2$, etc.
    \end{proof}

