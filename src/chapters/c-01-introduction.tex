\begin{figure}[!b]
  \centering
  \includegraphics[width=1\textwidth]{xkcd-intro-purete.png}
  \caption{Purity, Xkcd, n\up{o} 435\footnotemark}
  \vspace{2cm}
\end{figure}
\footnotetext{Extrait de \url{https://xkcd.com/435/}, original par Randall Munroe. --- Traduction: Domaines triés par pureté ; Du moins pur à gauche, au plus pur à droite\footnotemark; \emph{Psychologues:} \og{}La sociologie n'est que de la psychologie appliquée\fg{} ; \emph{Biologistes:} \og{}La psychologie n'est que de la biologie appliquée.\fg{} ; \emph{Chimistes:} \og{}La biologie n'est que de la chimie appliquée\fg{} ; \emph{Physiciens:} \og{}Ce qui n'est que de la physique appliquée. C'est sympa d'être au sommet\fg{} ; \emph{Mathématiciens:} \og{}Oh, eh les gars ! Je ne vous avais pas vus depuis ici!\fg{}}}
\footnotetext{Cette question, c.-à-d. \og{}quel domaine d'études est le plus pur\fg{}, est une question qui semble passionner certains étudiants --- voire certains chercheurs ---, et souvent la source de taquinages au sein de ces communautés. Randall Munroe en fait ici le constate en tournant cela, comme à son habitude, au ridicule.}

\chapter{Introduction}
  \addcontentsline{toc}{section}{Présentation de la mathématique du \textsc{xix}\up{e}}
  \addcontentsline{toc}{subsection}{Brève historique}
  Que ne donnerait-pas Randall Munroe pour blaguer de nos amis de la mathématique? Sa planche caricaturale \emph{Purity} m'en sera témoin: elle y est représentée hors des autres sciences, tant omniprésente que la physique même l'oublie, bien qu'elle en fasse usage sans répits.

  Le goût n'en a pas toujours été du même, et à vrai dire, pendant de nombreux siècles, on ne se souciait guère de savoir si l'on pratiquait la philosophie, la physique, ou encore la mathématique: une unique personne pouvait maîtriser l'une comme l'autre, comme s'ils étaient une et unies. Encore au \textsc{xvii}\up{e}, des savants comme Newton ou Leibniz\footnote{cf. \textsc{Baumann}, \emph{Histoire des mathématiques}, \url{http://www-irma.u-strasbg.fr/~baumann/polyh.pdf}} ont un apport considérable dans le domaine mathématique et
  physique, en développant tout deux le calcul infinitésimal --- mais de façon indépendante: on retrouve d'ailleurs aujourd'hui la \emph{notation de Leibniz} ($\frac{dy}{dx}$) et \emph{de Newton} ($\dot{y}$) --- afin de résoudre des questions de physique; Newton travaillera alors sur la notion de forces tandis que Leibniz mettra au point le concept d'énergie cinétique.

  Cela changera cependant dans la seconde moitié du \textsc{xviii}\up{e} siècle avec, par exemple, Cramer, qui propose son travail sur les courbes algébriques, Waring, sur la théorie des équations, ou encore Monge avec ses problèmes de géométrie descriptive et analytique.
 
  À ce moment-là, le problème \og{}à la mode\fg{} est de savoir si l'on peut trouver une formule permettant de résoudre une équation polynomiale de degré quelconque. 

  Abel\footnote{Neils Henrik \textsc{Abel}, Frindoë (1802) -- Froland (1829).} et Ruffini\footnote{Paolo \textsc{Ruffini}, Valentano (1765) -- Modène (1822).} y travaillent en parallèle, faisant usage de méthodes comparables, et montrent qu'il n'existe pas de solution au polynôme $$\mathcal{P}(x) = \sum^{k=n}_{k=0}{\alpha_kx^k}, \alpha_n \neq 0$$ pour un $n$ supérieur à $5$. Réfuté dans un premier temps, le travail d'Abel fut reconnu par la suite et reçut le grand prix de
  mathématiques en 1830, à titre posthume. Celui de Ruffini contenait une erreur manifeste et ne fut, par conséquent, jamais reconnu.

  Cependant, l'histoire ne s'arrête pas là. Un étudiant du nom d'Évariste Galois s'intéressa également à ce problème, alors qu'il n'était pas un mathématicien des plus brillants: il échoue à deux reprises à polytechnique, ses rédactions son obscures et loin d'être lisibles (on retrouve d'ailleurs un extrait de son \oe{}uvre en première page de ce document). Connaissant bien les récents travaux de Lagrange et de Cauchy, il va développer sa propre preuve au problème ---
  qui porte d'ailleurs anciennement le nom de \emph{théorème d'Abel-Ruffini} ou plus récemment \emph{de Galois} ---, en apportant une approche totalement nouvelle et innovante, la \emph{théorie de Galois}. 
  
  \begin{wrapfigure}{r}{0.3\textwidth}
      \includegraphics[width=0.3\textwidth]{galois}\\
      \caption{Unique portrait de Évariste Galois.}
  \end{wrapfigure}
  
  \addcontentsline{toc}{subsection}{L'apparition des idées de groupe}
  Cette théorie introduit des notions d'algèbre totalement nouvelles, parmi lesquelles figure pour la première fois dans l'histoire de la mathématique la \emph{théorie des groupes}.

  C'est Augustin Cauchy qui était chargé d'étudier les écrits de Galois. Cependant, celui-ci n'avait pas pour réputation d'être un homme d'une grande commodité:

  \say{Cauchy est infiniment catholique et bigot. Chose bien singulière pour un mathématicien! D'ailleurs, il est le seul qui travaille les mathématiques pures. Poisson, Fourier, Ampère, etc., s'occupent exclusivement de magnétisme et d'autres parties de la physique.} --- Lettre de \textsc{Abel} à \textsc{Hombolë}, octobre 1826\cf{1}{8}{con12}.
  
  Sa critique fut pourtant positive pour Galois, car, comme nous le montre Abel, bien que Cauchy soit très peu agréable, son travail est des plus reconnus, et les mathématiciens soutenus par celui-ci jouissent d'une plus grande renommée. 

  \begin{center}
    \includegraphics[width=.8\textwidth]{lettre-cauchy-crop}\\
    \emph{Extrait de la lettre d'excuse de présence de Cauchy à l'Académie des Sciences.}
  \end{center}

  En 1830, Galois présente son \emph{Mémoire sur la résolution des équations algébriques} à l'Institut de France afin de concourir pour le \emph{Grand prix de mathématiques}. Prix qu'Abel --- et non Galois --- gagnera à titre post-mortem; Galois, vexé, demanda à ce que son travail lui soit restitué. Ce ne fut jamais le cas car celui-ci avait été perdu à la mort de Fourier, alors que celui-ci n'avait jamais été inspecté. Pour cette raison, on n'a aucune trace du mémoire originel mais seulement ses brouillons, publiés à sa mort par Chevalier, accompagnés de son ultime lettre où il développe ses raisonnements au sujet de sa théorie.

  \say{Je me suis souvent hasardé dans ma vie à avancer des propositions dont je n'étais pas sûr; mais tout ce que j'ai écrit là est depuis bientôt un an dans ma tête, il est trop de mon intérêt de ne pas me tromper pour qu'on me soupçonne d'avoir énoncé des théorèmes dont je n'aurais pas la démonstration complète.} --- Lettre de \textsc{Galois} à \textsc{Chevalier}, mai 1832\cf{3}{415}{gc32}. 

  Un peu à la façon de la bande qui suit, Galois n'était pas conscient de l'entièreé des propriétés de ses idées: il dit, encore dans sa lettre à Chevalier\cf{4}{415}{gc32}, \say{Tu prieras publiquement Jacobi ou Gauss de donner leur avis, non sur la vérité, mais sur l'importance des théorèmes}. Cependant, ni l'un ni l'autre ne les consulta et il a fallu attendre Caley, qui, en 1854, publia un article dans le \emph{Philosophical magazine}, pour apporter la première définition formelle de
  groupe. 

  \begin{center}
    \includegraphics[width=0.5\textwidth]{xkcd-string-theory}\\
    \emph{This works on pretty much every level\footnote{Extrait de \url{https://xkcd.com/171/}, original par Randall Munroe. --- Traduction: La théorie des cordes résumée; \og{}Je viens d'avoir une idée géniale. Supposons que toute la matière et l'énergie soit faite de petites "cordes" vibrantes.\fg{}; \og{}Ok. Et qu'est-ce que ça impliquerait ?\fg{}; \og{}J'sais pas.\fg{}; \c Ca fonctionne à peu près à tous les niveaux.}.}
  \end{center}

  Fort heureusement, d'autres auteurs --- tels que Jordan avec un écrit majeur de formalisation en 1870, \og{}Traité des substitutions et des équations algébriques\fg{} --- s'y sont intéressés et ont fait progresser cette théorie, pour arriver aujourd'hui à un outil mathématique largement utilisé.

  \addcontentsline{toc}{section}{Buts du travail}
  \begin{imptext}
    Je me propose dans ce travail de retracer l'approche de Cauchy en y intégrant des éléments de théorie moderne, afin de démontrer à quel point son travail implique implicitement la notion de groupes.
  \end{imptext}
