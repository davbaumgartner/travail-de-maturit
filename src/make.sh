#!/bin/zsh

# Copyright (c) 2015 David Baugmartner <contact@davidbaumgartner.ch>

# For some reasons, Makefile doesn't work in this configuration.
# Maybe try to fix that ?

TEX="xelatex -interaction=nonstopmode"
IDX="makeindex"
BIB="biber"

MAIN="main"
CHAPTERS="chapters"

if [ "$1" != "clean" ]; then
  if [ "$1" != "quick" ]; then
    rm ${CHAPTERS}/chapters-list.tex
    ls ${CHAPTERS}/c-*.tex | awk '{ printf "\\input{%s}\n", $1 }' > ${CHAPTERS}/chapters-list.tex
    eval ${TEX} ${MAIN}.tex
    eval ${IDX} ${MAIN}.idx -s StyleInd.ist > logs/idx.log 2>&1
    eval ${BIB} ${MAIN} > logs/bib.log 2>&1
    eval ${TEX} ${MAIN}.tex
    eval ${TEX} ${MAIN}.tex
  fi
  eval ${TEX} ${MAIN}.tex | grep -v "(/usr/" > logs/tex.log 2>&1
  cp main.pdf ../out/main.pdf
else
  rm *.(aux|bbl|bcf|blg|ilg|ind|log|out|ptc|run.xml|toc|pdf|idx) ${CHAPTERS}/chapters-list.tex
  rm ../out/main.pdf
fi
