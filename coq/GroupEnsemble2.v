Require Import Setoid.
Require Import Ensembles.

Variable T : Type.

Variable G : Ensemble T.

Variable e : T.
Variable i : T -> T.
Variable c : T -> T -> T.

Variable H : Ensemble T.
Variable e' : T.
Variable i' : T -> T.
Variable c' : T -> T -> T.

Notation "x <+> y" := (c x y) (at level 50, left associativity).
Notation "x <*> y" := (c' x y) (at level 50, left associativity).

Inductive Group (G : Ensemble T) (e : T) (c : T -> T -> T) (i : T -> T) :=
  Group_Intro : (forall x : T, c x e = x) ->
                (forall x : T, c x (i x) = e) -> 
                (forall x y z : T, (c x (c y z)) = (c (c x y) z)) ->  
                Group G e c i.


Inductive AbelianGroup (G : Ensemble T) (e : T) (c : T -> T -> T) (i : T -> T) : Prop :=
  AbelianGroup_Intro : (Group G e c i) ->
                       (forall x y : T, x <+> y = y <+> x) ->
                       AbelianGroup G e c i.

Inductive SubGroup (G1 : Ensemble T) (e1 : T) (c1 : T -> T -> T) (i1 : T -> T) 
                   (G2 : Ensemble T) (e2 : T) (c2 : T -> T -> T) (i2 : T -> T) : Prop :=
  SubGroup_Intro : (Group G1 e1 c1 i1) ->
                   (Group G2 e2 c2 i2) ->
                   (forall x y : T, In T G1 x -> In T G2 y -> c1 x y = c2 x y) ->
                   SubGroup G1 e1 c1 i1 G2 e2 c2 i2.

Inductive NormalSubGroup (G1 : Ensemble T) (e1 : T) (c1 : T -> T -> T) (i1 : T -> T) 
                         (G2 : Ensemble T) (e2 : T) (c2 : T -> T -> T) (i2 : T -> T) :=
  NormalSubGroup_Intro : (SubGroup G1 e1 c1 i1 G2 e2 c2 i2) ->
                         (forall (a x : T), In T G2 x ->
                                            In T G1 a ->
                                            In T G2 (a <+> x <+> i1 a)) ->
                         NormalSubGroup G1 e1 c1 i1 G2 e2 c2 i2.

Inductive HomeomorphicGroup (G1 : Ensemble T) (e1 : T) (c1 : T -> T -> T) (i1 : T -> T) 
                            (G2 : Ensemble T) (e2 : T) (c2 : T -> T -> T) (i2 : T -> T) : Prop :=
  HomeomorphicGroup_Intro : (Group G1 e1 c1 i1) ->
                            (Group G2 e2 c2 i2) ->
                            (forall x y : T, exists (h : T -> T), h (x <+> y) = (h x) <*> (h y)) ->
                            HomeomorphicGroup G1 e1 c1 i1 G2 e2 c2 i2.

Inductive IsomorphicGroup (G1 : Ensemble T) (e1 : T) (c1 : T -> T -> T) (i1 : T -> T) 
                          (G2 : Ensemble T) (e2 : T) (c2 : T -> T -> T) (i2 : T -> T) : Prop :=
  IsomorphicGroup_Intro : (HomeomorphicGroup G1 e1 c1 i1 G2 e2 c2 i2) ->
                          (HomeomorphicGroup G2 e2 c2 i2 G1 e1 c1 i1) ->
                          IsomorphicGroup G1 e1 c1 i1 G2 e2 c2 i2.

Lemma unique_id : forall a,
                    Group G e c i ->
                    a <+> a = a ->
                    a = e.
Proof.
  intros a Group H.
  inversion Group as [id_r inv_r assoc].
  rewrite <- (id_r a).
  rewrite <- (inv_r a).
  rewrite assoc.
  rewrite H.
  reflexivity.
Qed.

Lemma inv_l : forall a,
                Group G e c i ->
                i a <+> a = e.
Proof.
  intros a Group.
  inversion Group as [id_r inv_r assoc].
  apply unique_id.
  apply Group.
  rewrite <- (assoc (i a) a (i a <+> a)).
  rewrite (assoc a (i a) a).
  rewrite inv_r.
  rewrite assoc.
  rewrite id_r.
  reflexivity.
Qed.

Lemma id_l : forall a,
               Group G e c i ->
               e <+> a = a.
Proof.
  intros a Group.
  inversion Group as [id_r inv_r assoc].
  rewrite <- (inv_r a).
  rewrite <- assoc.
  rewrite inv_l.
  rewrite id_r.
  reflexivity.
  apply Group.
Qed.

Lemma cancel_r : forall a b u,
                   Group G e c i ->
                   a <+> u = b <+> u -> a = b.
Proof.
  intros a b u Group H.
  inversion Group as [id_r inv_r assoc].
  rewrite <- (id_r a).
  rewrite <- (id_r b).
  rewrite <- (inv_r u).
  rewrite assoc.
  rewrite assoc.
  rewrite H.
  reflexivity.
Qed.

Lemma cancel_l : forall a b u, 
                   Group G e c i ->
                   u <+> a = u <+> b -> a = b.
Proof.
  intros a b u Group H.
  inversion Group as [id_r inv_r assoc].
  rewrite <- (id_l a).
  rewrite <- (id_l b).
  rewrite <- (inv_l u).
  rewrite <- assoc.
  rewrite <- assoc.
  rewrite H.
  reflexivity.
  apply Group.
  apply Group.
  apply Group.
Qed.

Lemma e_uniq_l : forall a p, 
                   Group G e c i ->
                   p <+> a = a -> p = e.
Proof.
  intros a p Group H.
  inversion Group as [id_r inv_r assoc].
  rewrite (cancel_r p e a).
  reflexivity.
  apply Group.
  rewrite id_l.
  apply H.
  apply Group.
Qed.

Lemma inv_uniq_l : forall a b, 
                     Group G e c i ->
                     a <+> b = e -> a = i b.
Proof.
  intros a b Group H.
  inversion Group as [id_r inv_r assoc].
  rewrite (cancel_r a (i b) b).
  reflexivity.
  apply Group.
  rewrite inv_l.
  apply H.
  apply Group.
Qed.

Lemma e_uniq_r : forall a p, 
                   Group G e c i ->
                   a <+> p = a -> p = e.
Proof.
  intros a p Group H.
  inversion Group as [id_r inv_r assoc].
  rewrite (cancel_l p e a).
  reflexivity.
  apply Group.
  rewrite id_r.
  apply H.
Qed.

Lemma inv_uniq_r : forall a b, 
                     Group G e c i ->
                     a <+> b = e -> b = i a.
Proof.
  intros a b Group H.
  inversion Group as [id_r inv_r assoc].
  rewrite (cancel_l b (i a) a).
  reflexivity.
  apply Group.
  rewrite inv_r.
  apply H.
Qed.

Lemma inv_distr : forall a b, 
                    Group G e c i ->
                    i (a <+> b) = i b <+> i a.
Proof.
  intros a b Group.
  inversion Group as [id_r inv_r assoc].
  rewrite (cancel_r (i (a <+> b)) (i b <+> i a) (a <+> b)).
  reflexivity.
  apply Group.
  rewrite inv_l.
  rewrite <- assoc.
  rewrite assoc with (i a) a b.
  rewrite inv_l.
  rewrite id_l.
  rewrite inv_l.
  reflexivity.
  apply Group.
  apply Group.
  apply Group.
  apply Group.
Qed.

Lemma double_inv : forall a, 
                     Group G e c i ->
                     i (i a) = a.
Proof.
  intros a Group.
  inversion Group as [id_r inv_r assoc].
  rewrite (cancel_r (i (i a)) a (i a)).
  reflexivity.
  apply Group.
  rewrite <- (inv_distr).
  rewrite inv_r.
  rewrite (cancel_r (i e) e e).
  reflexivity.
  apply Group.
  rewrite inv_l.
  rewrite (id_r e).
  reflexivity.
  apply Group.
  apply Group.
Qed.

Lemma id_inv : Group G e c i -> 
               i e = e.
Proof.
  intros Group.
  inversion Group as [id_r inv_r assoc].
  rewrite <- (inv_l e).
  rewrite inv_distr.
  rewrite double_inv.
  reflexivity.
  apply Group.
  apply Group.
  apply Group.
Qed.

Lemma subgroup_cond_i : forall x y,
                          (forall u : T, H u -> G u) ->
                          Group G e c i ->
                          (H x -> H y -> H (x <+> y)) ->
                          (H x -> H (i x)) ->
                          SubGroup G e c i H e' c' i'.
Proof.
Admitted.

Lemma subgroup_cond_ii : forall x y,
                           (forall u : T, H u -> G u) ->
                           Group G e c i ->
                           (H (x <+> (i y))) ->
                           SubGroup G e c i H e c i.
Proof.
  intros x y SG GroupG.
  inversion GroupG as [id_r inv_r assoc].
  split.
  apply GroupG.
  assert (GroupH: Group H e c i).
    split.
    apply id_r.
    apply inv_r.
    apply assoc.
  apply GroupH.
  reflexivity.
Qed.

Lemma subgroup_i_eq : SubGroup G e c i H e' c' i' ->
                      forall x, i x = i' x.
Proof.
  intros SubGroup x.
  inversion SubGroup as [GroupG GroupH c_eq].
  inversion GroupG as [Gid_r Ginv_r Gassoc].
  inversion GroupH as [Hid_r Hinv_r Hassoc].
  rewrite (cancel_l (i x) (i' x) x).
  reflexivity.
  apply GroupG.
  rewrite Ginv_r.
  rewrite c_eq.
  rewrite Hinv_r. 
Admitted.
  


Lemma subgroup_e_eq : SubGroup G e c i H e' c' i' ->
                      e = e'.
Proof.
  intros SubGroup.
  inversion SubGroup as [GroupG GroupH c_eq].
  inversion GroupG as [Gid_r Ginv_r Gassoc].
  inversion GroupH as [Hid_r Hinv_r Hassoc].
  rewrite <- (Ginv_r e).
  rewrite <- (Hinv_r e).
Admitted.

  

Lemma normal_abelian_subgroup : AbelianGroup G e c i ->
                                SubGroup G e c i H e' c' i' ->
                                NormalSubGroup G e c i H e' c' i'.
Proof.
  intros AbelianGroup SubGroup.
  inversion AbelianGroup as [GroupG Gcomm].
  inversion SubGroup as [_ GroupH HGsubset].
  inversion GroupG as [Gid_r Ginv_r Gassoc].
  inversion GroupH as [Hid_r Hinv_r Hassoc].
  split.
  apply SubGroup.
  intros a x U V.
  rewrite Gcomm.
  rewrite Gassoc.
  rewrite inv_l.
  rewrite id_l.
  apply U.
  apply GroupG.
  apply GroupG.
Qed.
