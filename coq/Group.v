Require Import Setoid.

Variable G : Set.

Variable e : G.
Variable i : G -> G.
Variable c : G -> G -> G.

Variable H : Set.
Variable e' : H.
Variable i' : H -> H.
Variable c' : H -> H -> H.

Variable inj_H_G : H -> G.
Axiom inj_H_G_inj : forall x y : H, inj_H_G x = inj_H_G y -> x = y.
Coercion inj_H_G : H >-> G.

Notation "x <+> y" := (c x y) (at level 50, left associativity).
Notation "x <*> y" := (c' x y) (at level 50, left associativity).

Inductive Group (G : Set) (e : G) (c : G -> G -> G) (i : G -> G) :=
  Group_Intro : (forall x : G, c x e = x) ->
                (forall x : G, c x (i x) = e) -> 
                (forall x y z : G, (c x (c y z)) = (c (c x y) z)) ->  
                Group G e c i.

Inductive AbelianGroup (G : Set) (e : G) (c : G -> G -> G) (i : G -> G) : Prop :=
  AbelianGroup_Intro : (Group G e c i) ->
                       (forall x y : G, c x y = c y x) ->
                       AbelianGroup G e c i.

Inductive SubGroup (G1 : Set) (e1 : G1) (c1 : G1 -> G1 -> G1) (i1 : G1 -> G1) 
                   (G2 : Set) (e2 : G2) (c2 : G2 -> G2 -> G2) (i2 : G2 -> G2) : Prop :=
  SubGroup_Intro : (Group G1 e1 c1 i1) ->
                   (Group G2 e2 c2 i2) ->
                   (forall (x y : G2) (f : G2 -> G1), f x = f y -> x = y) ->
                   SubGroup G1 e1 c1 i1 G2 e2 c2 i2.

Inductive NormalSubGroup (G1 : Set) (e1 : G1) (c1 : G1 -> G1 -> G1) (i1 : G1 -> G1) 
                         (G2 : Set) (e2 : G2) (c2 : G2 -> G2 -> G2) (i2 : G2 -> G2) :=
  NormalSubGroup_Intro : (SubGroup G1 e1 c1 i1 G2 e2 c2 i2) ->
                         (forall (a : G1) (x y : G2) (f : G2 -> G1) (g : G1 -> G2), (forall n, n = g (f n)) ->
                                                                                    g (c1 (c1 a (f x)) (i1 a)) = g (c1 (c1 a (f y)) (i1 a)) ->
                                                                                                           x = y) ->
                         NormalSubGroup G1 e1 c1 i1 G2 e2 c2 i2.

Inductive HomeomorphicGroup (G1 : Set) (e1 : G1) (c1 : G1 -> G1 -> G1) (i1 : G1 -> G1) 
                            (G2 : Set) (e2 : G2) (c2 : G2 -> G2 -> G2) (i2 : G2 -> G2) : Prop :=
  HomeomorphicGroup_Intro : (Group G1 e1 c1 i1) ->
                            (Group G2 e2 c2 i2) ->
                            (forall x y : G1, exists (h : G1 -> G2), h (c1 x y) = c2 (h x) (h y)) ->
                            HomeomorphicGroup G1 e1 c1 i1 G2 e2 c2 i2.

Inductive IsomorphicGroup (G1 : Set) (e1 : G1) (c1 : G1 -> G1 -> G1) (i1 : G1 -> G1) 
                          (G2 : Set) (e2 : G2) (c2 : G2 -> G2 -> G2) (i2 : G2 -> G2) : Prop :=
  IsomorphicGroup_Intro : (HomeomorphicGroup G1 e1 c1 i1 G2 e2 c2 i2) ->
                          (HomeomorphicGroup G2 e2 c2 i2 G1 e1 c1 i1) ->
                          IsomorphicGroup G1 e1 c1 i1 G2 e2 c2 i2.


Lemma unique_id : forall a,
                    Group G e c i ->
                    a <+> a = a ->
                    a = e.
Proof.
  intros a Group H.
  inversion Group as [id_r inv_r assoc].
  rewrite <- (id_r a).
  rewrite <- (inv_r a).
  rewrite assoc.
  rewrite H.
  reflexivity.
Qed.

Lemma inv_l : forall a,
                Group G e c i ->
                i a <+> a = e.
Proof.
  intros a Group.
  inversion Group as [id_r inv_r assoc].
  apply unique_id.
  apply Group.
  rewrite <- (assoc (i a) a (i a <+> a)).
  rewrite (assoc a (i a) a).
  rewrite inv_r.
  rewrite assoc.
  rewrite id_r.
  reflexivity.
Qed.

Lemma id_l : forall a,
               Group G e c i ->
               e <+> a = a.
Proof.
  intros a Group.
  inversion Group as [id_r inv_r assoc].
  rewrite <- (inv_r a).
  rewrite <- assoc.
  rewrite inv_l.
  rewrite id_r.
  reflexivity.
  apply Group.
Qed.

Lemma cancel_r : forall a b u,
                   Group G e c i ->
                   a <+> u = b <+> u -> a = b.
Proof.
  intros a b u Group H.
  inversion Group as [id_r inv_r assoc].
  rewrite <- (id_r a).
  rewrite <- (id_r b).
  rewrite <- (inv_r u).
  rewrite assoc.
  rewrite assoc.
  rewrite H.
  reflexivity.
Qed.

Lemma cancel_l : forall a b u, 
                   Group G e c i ->
                   u <+> a = u <+> b -> a = b.
Proof.
  intros a b u Group H.
  inversion Group as [id_r inv_r assoc].
  rewrite <- (id_l a).
  rewrite <- (id_l b).
  rewrite <- (inv_l u).
  rewrite <- assoc.
  rewrite <- assoc.
  rewrite H.
  reflexivity.
  apply Group.
  apply Group.
  apply Group.
Qed.

Lemma e_uniq_l : forall a p, 
                   Group G e c i ->
                   p <+> a = a -> p = e.
Proof.
  intros a p Group H.
  inversion Group as [id_r inv_r assoc].
  rewrite (cancel_r p e a).
  reflexivity.
  apply Group.
  rewrite id_l.
  apply H.
  apply Group.
Qed.

Lemma inv_uniq_l : forall a b, 
                     Group G e c i ->
                     a <+> b = e -> a = i b.
Proof.
  intros a b Group H.
  inversion Group as [id_r inv_r assoc].
  rewrite (cancel_r a (i b) b).
  reflexivity.
  apply Group.
  rewrite inv_l.
  apply H.
  apply Group.
Qed.

Lemma e_uniq_r : forall a p, 
                   Group G e c i ->
                   a <+> p = a -> p = e.
Proof.
  intros a p Group H.
  inversion Group as [id_r inv_r assoc].
  rewrite (cancel_l p e a).
  reflexivity.
  apply Group.
  rewrite id_r.
  apply H.
Qed.

Lemma inv_uniq_r : forall a b, 
                     Group G e c i ->
                     a <+> b = e -> b = i a.
Proof.
  intros a b Group H.
  inversion Group as [id_r inv_r assoc].
  rewrite (cancel_l b (i a) a).
  reflexivity.
  apply Group.
  rewrite inv_r.
  apply H.
Qed.

Lemma inv_distr : forall a b, 
                    Group G e c i ->
                    i (a <+> b) = i b <+> i a.
Proof.
  intros a b Group.
  inversion Group as [id_r inv_r assoc].
  rewrite (cancel_r (i (a <+> b)) (i b <+> i a) (a <+> b)).
  reflexivity.
  apply Group.
  rewrite inv_l.
  rewrite <- assoc.
  rewrite assoc with (i a) a b.
  rewrite inv_l.
  rewrite id_l.
  rewrite inv_l.
  reflexivity.
  apply Group.
  apply Group.
  apply Group.
  apply Group.
Qed.

Lemma double_inv : forall a, 
                     Group G e c i ->
                     i (i a) = a.
Proof.
  intros a Group.
  inversion Group as [id_r inv_r assoc].
  rewrite (cancel_r (i (i a)) a (i a)).
  reflexivity.
  apply Group.
  rewrite <- (inv_distr).
  rewrite inv_r.
  rewrite (cancel_r (i e) e e).
  reflexivity.
  apply Group.
  rewrite inv_l.
  rewrite (id_r e).
  reflexivity.
  apply Group.
  apply Group.
Qed.

Lemma id_inv : Group G e c i -> 
               i e = e.
Proof.
  intros Group.
  inversion Group as [id_r inv_r assoc].
  rewrite <- (inv_l e).
  rewrite inv_distr.
  rewrite double_inv.
  reflexivity.
  apply Group.
  apply Group.
  apply Group.
Qed.

Lemma c_subgroup_common : SubGroup G e c i H e' c' i' ->
                          forall (x y : H) (u v : G) (f : H -> G) (g : G -> H),
                            (forall n : H, n = g (f n)) ->
                            f (c' x y) = (c u v) ->
                            g (c u v) = c' x y.
Proof.
  intros SubGroup Hx Hy Gx Gy f g Hf Hc.
  rewrite <- Hc.
  rewrite Hf.
  reflexivity.
Qed.

Lemma normal_abelian_subgroup : AbelianGroup G e c i ->
                                SubGroup G e c i H e' c' i' ->
                                NormalSubGroup G e c i H e' c' i'.
Proof.
  intros AbelianGroup SubGroup.
  inversion AbelianGroup as [GroupG Gcomm].
  inversion SubGroup as [_ GroupH HGsubset].
  inversion GroupG as [Gid_r Ginv_r Gassoc].
  inversion GroupH as [Hid_r Hinv_r Hassoc].
  split.
  apply SubGroup.
  intros a x y f g T.
  rewrite Gcomm.
  rewrite Gassoc.
  rewrite inv_l.
  rewrite id_l.
  rewrite <- T with (n := x).
  rewrite Gcomm.
  rewrite Gassoc.
  rewrite inv_l.
  rewrite id_l.
  rewrite <- T with (n := y).
  auto.
  apply GroupG.
  apply GroupG.
  apply GroupG.
  apply GroupG.
Qed.
