Require Import List.

Import ListNotations.

Inductive PermutableEnsembles {A : Type} (l1 l2 : list A) :=
  PermutaleEnsembles_Intro : (forall x : A, In x l1 -> In x l2) ->
                             (forall x : A, In x l2 -> In x l1) ->
                             PermutableEnsembles l1 l2.

Require GroupEnsemble2.

Inductive Transposition {A : Type} (l1 l2 : list A) :=
  Transposition_Intro : (PermutableEnsembles l1 l2) -> 
                        (exists i1 i2, i1 <> i2 /\ (forall z, nth i1 l1 z = nth i2 l2 z) /\
                        (forall i, i <> i1 /\ i <> i2 -> (forall z, nth i l1 z = nth i l2 z))) ->
                        Transposition l1 l2.

Example trans_1:  Transposition [ 1 ; 2 ; 3 ; 4] [2 ; 1 ; 3 ; 4].
Proof.
  split.
  split.
  induction x.
  unfold In.
  tauto.
  unfold In.
  tauto.
  induction x.
  unfold In.
  tauto.
  unfold In.
  tauto.
  exists 0.
  exists 1.
  intuition.
  apply O_S in H.
  apply H.
  induction i.
  tauto.
  induction i.
  tauto.
  unfold nth.
  auto.
Qed.

Inductive Permutation {A : Type} (l1 l2 : list A) :=
  Permutation_Intro : (PermutableEnsembles l1 l2) ->
                      Permutation l1 l2.

Defintion permute (l1 l2 : list A) (x : A) :=
  

Example tramp: Permutation [ 1 ; 2 ; 3 ; 4 ; 5 ] [ 3 ; 1 ; 2 ; 5 ; 4 ] <-> Transposition 